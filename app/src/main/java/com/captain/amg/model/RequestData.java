package com.captain.amg.model;

import com.google.gson.annotations.SerializedName;

public class RequestData {

    @SerializedName("serviceId")
    private String serviceId;
    @SerializedName("userId")
    private String userId;

    public RequestData(String serviceId, String userId) {
        this.serviceId = serviceId;
        this.userId = userId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
