package com.captain.amg.model;

import com.google.gson.annotations.SerializedName;

public class Locations {

    @SerializedName("FIIDUBICACION")    private int fiidUbicacion;
    @SerializedName("FIIDCODIGOPOSTAL") private int fiidCP;
    @SerializedName("FCNOMBRE")         private String fcNombre;
    @SerializedName("FCCLAVEUBICACION") private String fcClaveUbicacion;
    @SerializedName("FCDIRECCION")      private String fcDireccion;
    @SerializedName("FNLATITUD")        private Double fnLatitud;
    @SerializedName("FNLONGITUD")       private Double fnLongitud;
    @SerializedName("FIGEOCERCA")       private int fiGeocerca;
    @SerializedName("FIIDESTATUS")      private int fiidEstatus;
    @SerializedName("FIIDPOLIGONO")     private int fiidPoligono;

    public Locations(int fiidUbicacion, int fiidCP, String fcNombre, String fcClaveUbicacion,
                     String fcDireccion, Double fnLatitud, Double fnLongitud,
                     int fiGeocerca, int fiidEstatus, int fiidPoligono) {
        this.fiidUbicacion = fiidUbicacion;
        this.fiidCP = fiidCP;
        this.fcNombre = fcNombre;
        this.fcClaveUbicacion = fcClaveUbicacion;
        this.fcDireccion = fcDireccion;
        this.fnLatitud = fnLatitud;
        this.fnLongitud = fnLongitud;
        this.fiGeocerca = fiGeocerca;
        this.fiidEstatus = fiidEstatus;
        this.fiidPoligono = fiidPoligono;
    }

    public int getFiidUbicacion() {
        return fiidUbicacion;
    }

    public void setFiidUbicacion(int fiidUbicacion) {
        this.fiidUbicacion = fiidUbicacion;
    }

    public int getFiidCP() {
        return fiidCP;
    }

    public void setFiidCP(int fiidCP) {
        this.fiidCP = fiidCP;
    }

    public String getFcNombre() {
        return fcNombre;
    }

    public void setFcNombre(String fcNombre) {
        this.fcNombre = fcNombre;
    }

    public String getFcClaveUbicacion() {
        return fcClaveUbicacion;
    }

    public void setFcClaveUbicacion(String fcClaveUbicacion) {
        this.fcClaveUbicacion = fcClaveUbicacion;
    }

    public String getFcDireccion() {
        return fcDireccion;
    }

    public void setFcDireccion(String fcDireccion) {
        this.fcDireccion = fcDireccion;
    }

    public Double getFnLatitud() {
        return fnLatitud;
    }

    public void setFnLatitud(Double fnLatitud) {
        this.fnLatitud = fnLatitud;
    }

    public Double getFnLongitud() {
        return fnLongitud;
    }

    public void setFnLongitud(Double fnLongitud) {
        this.fnLongitud = fnLongitud;
    }

    public int getFiGeocerca() {
        return fiGeocerca;
    }

    public void setFiGeocerca(int fiGeocerca) {
        this.fiGeocerca = fiGeocerca;
    }

    public int getFiidEstatus() {
        return fiidEstatus;
    }

    public void setFiidEstatus(int fiidEstatus) {
        this.fiidEstatus = fiidEstatus;
    }

    public int getFiidPoligono() {
        return fiidPoligono;
    }

    public void setFiidPoligono(int fiidPoligono) {
        this.fiidPoligono = fiidPoligono;
    }
}
