package com.captain.amg.persistence.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.captain.amg.persistence.roomdatabase.dao.PersonalDataDao;
import com.captain.amg.persistence.roomdatabase.db.PersonalDataRoomDatabase;
import com.captain.amg.persistence.roomdatabase.entity.PersonalData;

import java.util.List;

public class PersonalDataRepository {

    public PersonalDataDao mPersonalDataDao;
    private LiveData<List<PersonalData>> mAllData;

    public PersonalDataRepository(Application application) {
        PersonalDataRoomDatabase db = PersonalDataRoomDatabase.getDatabase(application);
        mPersonalDataDao = db.personalDataDao();
        mAllData = mPersonalDataDao.getAllData();
    }

    public LiveData<List<PersonalData>> getAllData() {
        return mAllData;
    }

    public void insert(PersonalData personalData) {
        new insertAsyncTask(mPersonalDataDao).execute(personalData);
    }

    public void deleteAll() {
        new deleteAllAsyncTask(mPersonalDataDao).execute();
    }

    private class insertAsyncTask extends AsyncTask<PersonalData, Void, Void> {

        private PersonalDataDao mAsyncTaskDao;

        insertAsyncTask(PersonalDataDao mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(final PersonalData... personalData) {
            mAsyncTaskDao.insert(personalData[0]);
            return null;
        }
    }

    private class deleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private PersonalDataDao mAsyncTaskDao;

        public deleteAllAsyncTask(PersonalDataDao mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }
}
