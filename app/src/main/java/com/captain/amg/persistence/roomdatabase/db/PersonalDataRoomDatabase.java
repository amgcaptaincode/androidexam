package com.captain.amg.persistence.roomdatabase.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.captain.amg.persistence.roomdatabase.dao.PersonalDataDao;
import com.captain.amg.persistence.roomdatabase.entity.PersonalData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Database(entities = {PersonalData.class}, version = 1)
public abstract class PersonalDataRoomDatabase extends RoomDatabase {

    public abstract PersonalDataDao personalDataDao();

    private static volatile PersonalDataRoomDatabase INSTANCE;

    public static PersonalDataRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (PersonalDataRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            PersonalDataRoomDatabase.class, "personal_data_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            //new PopulateDbAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final PersonalDataDao mDao;

        public PopulateDbAsync(PersonalDataRoomDatabase db) {
            mDao = db.personalDataDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
            String dateInString = "08/12/1990";
            Date date = null;
            try {
                date = formatter.parse(dateInString);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            mDao.deleteAll();
            PersonalData word = new PersonalData("Miguel Cervantes", new Date(), "Desarrollador");
            mDao.insert(word);
            return null;
        }
    }
}

        /*
        * Nombre
Fecha nacimiento
Puesto
Miguel Cervantes
08-Dic-1990
Desarrollador
Juan Morales
03-Jul-1990
Desarrollador
Roberto Méndez
14-Oct-1990
Desarrollador
Miguel Cuevas
08-Dic-1990
Desarrollador
*/