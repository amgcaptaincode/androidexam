package com.captain.amg.persistence.roomdatabase.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.captain.amg.persistence.roomdatabase.converters.DateConverter;

import java.util.Date;

@Entity(tableName = "personal_data_table")
@TypeConverters(DateConverter.class)
public class PersonalData {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int uuid;
    private String name;
    private Date birthdate;
    private String post;

    public PersonalData(String name, Date birthdate, String post) {
        this.name = name;
        this.birthdate = birthdate;
        this.post = post;
    }

    @NonNull
    public int getUuid() {
        return uuid;
    }

    public void setUuid(@NonNull int uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }
}
