package com.captain.amg.persistence.roomdatabase.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.captain.amg.persistence.roomdatabase.entity.PersonalData;

import java.util.List;

@Dao
public interface PersonalDataDao {

    @Insert
    void insert(PersonalData personalData);

    @Query("SELECT * FROM personal_data_table ORDER BY name ASC")
    LiveData<List<PersonalData>> getAllData();

    @Query("DELETE FROM personal_data_table")
    void deleteAll();

}
