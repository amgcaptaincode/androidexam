package com.captain.amg.manager;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import okhttp3.ResponseBody;

public class ZipManager {

    private static final String TAG = ZipManager.class.getSimpleName();

    public static boolean saveToDisk(ResponseBody body, String path) {
        try {
            new File(path).mkdir();
            File destinationFile = new File(path.concat("/captain.zip"));

            InputStream is = null;
            OutputStream os = null;

            try {
                Log.d(TAG, "File Size = " + body.contentLength());

                is = body.byteStream();
                os = new FileOutputStream(destinationFile);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                byte data[] = new byte[4096];
                int count;
                int progress = 0;
                while ((count = is.read(data)) != -1) {
                    baos.write(data, 0, count);
                    byte[] bytes = baos.toByteArray();
                    //os.write(data, 0, count);
                    os.write(bytes);
                    baos.reset();
                    progress +=count;
                    Log.d(TAG, "Progress: " + progress
                            + "/" + body.contentLength() + " >>>> "
                            + (float) progress/body.contentLength());
                }

                os.flush();
                Log.d(TAG, "File saved successfully!");
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Failed to save the file!");
                return false;
            } finally {
                if (is != null) is.close();
                if (os != null) os.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "Failed to save the file!");
            return false;
        }
    }

    public static String unzip(String zipFile, String location) {
        try {
            File f = new File(location);
            if (!f.isDirectory()) {
                f.mkdirs();
            }
            ZipInputStream zin = new ZipInputStream(new FileInputStream(zipFile));
            try {
                ZipEntry ze = null;
                String pathFile = null;
                while ((ze = zin.getNextEntry()) != null) {
                    Log.d(TAG, ze.getName());
                    String path = location + File.separator + ze.getName();

                    if (ze.isDirectory()) {
                        File unzipFile = new File(path);
                        if (!unzipFile.isDirectory())
                            unzipFile.mkdirs();
                    } else {
                        Log.d(TAG, "Unzip start...");
                        FileOutputStream fout = new FileOutputStream(path, false);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        byte[] buffer = new byte[4096];

                        int count;
                        // reading and writing
                        while ((count = zin.read(buffer)) != -1) {
                            baos.write(buffer, 0, count);
                            byte[] bytes = baos.toByteArray();
                            fout.write(bytes);
                            baos.reset();
                        }

                        fout.close();
                        zin.closeEntry();
                        /*try {
                            for (int c = zin.read(); c != -1; c = zin.read()) {
                                fout.write(c);
                            }
                            zin.closeEntry();
                            Log.d(TAG, "Unzip successfully");

                        } finally {
                            fout.close();
                        }*/
                    }
                    Log.d(TAG, "Unzip successfully");
                    Log.d(TAG, path);
                    pathFile = path;
                }
                return pathFile;
            } finally {
                zin.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Unzip exception", e);
            return "-1";
        }
    }

    public static String loadFile(String pathFile) {

        //Get the text file
        File file = new File(pathFile);

        //Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
            }
            br.close();
            Log.d(TAG, "Load file successfully!");
            return text.toString();
        }
        catch (IOException e) {
            Log.d(TAG, e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public static String loadJson(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("file.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

            Log.v("--->", "Json reader");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
