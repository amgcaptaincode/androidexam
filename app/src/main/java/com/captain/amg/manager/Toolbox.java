package com.captain.amg.manager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Toolbox {

    public static Date getDate(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", new Locale("es", "MX"));
        try {
            Date date = formatter.parse(dateString);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String dateToString(Date date) {
        DateFormat dateFormat =
                new SimpleDateFormat(
                        "yyyy-MMM-dd",
                        new Locale("es", "MX"));
        return dateFormat.format(date);
    }

}
