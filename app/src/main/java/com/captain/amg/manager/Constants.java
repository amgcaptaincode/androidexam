package com.captain.amg.manager;

public class Constants {

    public static final String BASE_URL = "http://upaxdev.com/ws/webresources/generic/";

    public static final int DELAY = 2000;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 745;
}
