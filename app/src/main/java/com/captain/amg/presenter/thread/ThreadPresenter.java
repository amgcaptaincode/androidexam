package com.captain.amg.presenter.thread;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

public class ThreadPresenter implements ThreadPresenterInterface{

    private static final String TAG = ThreadPresenter.class.getSimpleName();

    private ThreadViewInterface threadViewInterface;

    public ThreadPresenter(ThreadViewInterface threadViewInterface) {
        this.threadViewInterface = threadViewInterface;
    }

    @Override
    public void executeThread(int seconds) {
        new MyThread().execute(seconds);
    }

    private class MyThread extends AsyncTask<Integer, Integer, String>{

        @Override
        protected String doInBackground(Integer... integers) {
            for (int count = 1; count <= integers[0]; count++) {
                try {
                    Thread.sleep(1000);
                    publishProgress(count);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return "Tarea Finalizada";
        }

        @Override
        protected void onPostExecute(String s) {
            threadViewInterface.showResponse(s);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            threadViewInterface.setProgressThread(values[0]);
        }
    }



}
