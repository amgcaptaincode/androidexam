package com.captain.amg.presenter.thread;

public interface ThreadPresenterInterface {

    void executeThread(int seconds);
}
