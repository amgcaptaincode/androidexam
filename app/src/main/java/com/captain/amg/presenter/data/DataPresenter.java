package com.captain.amg.presenter.data;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.captain.amg.manager.ZipManager;
import com.captain.amg.model.RequestData;
import com.captain.amg.model.ResponseData;
import com.captain.amg.model.Locations;
import com.captain.amg.network.NetworkClient;
import com.captain.amg.network.NetworkInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

import static com.captain.amg.manager.ZipManager.loadFile;
import static com.captain.amg.manager.ZipManager.saveToDisk;
import static com.captain.amg.manager.ZipManager.unzip;

public class DataPresenter implements DataPresenterInterface {

    private static final String TAG = DataPresenter.class.getSimpleName();
    private DataViewInterface dataViewInterface;
    private Context context;

    public DataPresenter(DataViewInterface mainViewInterface, Context context) {
        this.dataViewInterface = mainViewInterface;
        this.context = context;
    }

    @Override
    public void getData(Activity parent, RequestData requestData) {
        showStatus(parent, "Descargando Ubicaciones" );
        getObservableData(requestData).subscribeWith(getObserverData());
    }

    private Single<ResponseData> getObservableData(RequestData requestData) {
        return NetworkClient.getRetrofit()
                .create(NetworkInterface.class)
                .getData(requestData)
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver getObserverData() {
        return new DisposableSingleObserver<ResponseData>() {
            @Override
            public void onSuccess(ResponseData responseData) {
                Log.d(TAG, responseData.getValueResponse());
                downloadFileZip(responseData.getValueResponse());
            }

            @Override
            public void onError(Throwable e) {
                dataViewInterface.showError(messageError(e));
            }
        };
    }

    @Override
    public void downloadFileZip(String urlFile) {
        getObservableDownload(urlFile).subscribeWith(getObserverDownload());
    }

    private Single<ResponseBody> getObservableDownload(String urlFile) {
        return NetworkClient.getRetrofit()
                .create(NetworkInterface.class)
                .downloadZipFile(urlFile)
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver getObserverDownload() {
        return new DisposableSingleObserver<ResponseBody>() {
            @Override
            public void onSuccess(final ResponseBody responseBody) {
                Log.d(TAG, "ResponseBody " + responseBody);
                saveFile(responseBody);
            }

            @Override
            public void onError(Throwable e) {
                messageError(e);
            }
        };
    }

    private void saveFile(ResponseBody responseBody) {
        new SaveFileZip().execute(responseBody);
    }

    private void unzipF() {
        String path = context.getFilesDir().getAbsolutePath().concat("/zips");
        final File destinationFile = new File(path.concat("/captain.zip"));
        new UnzipFileDownload().execute(destinationFile.getPath());

    }

    private void writeReadFile(ResponseBody responseBody) {
        String path = context.getFilesDir().getAbsolutePath().concat("/zips");
        boolean isSave = saveToDisk(responseBody, path);

        final File destinationFile = new File(path.concat("/captain.zip"));
        String pathFile = null;
        if (isSave)
        {
            pathFile = unzipFile(destinationFile.getPath());
            if (pathFile != null && !pathFile.equals("-1")) {
                String json = loadFile(pathFile);
                if (json != null)
                    dataViewInterface.showData(loadLocations(json));
                else
                    dataViewInterface.showError("Ubicaciones no disponibles");
            }
        }
    }

    @Override
    public void getLocationsFromAssets(Activity parent) {
        showStatus(parent, "Cargando Ubicaciones desde Assests" );
        getLocations();
    }

    public void getLocations() {
        new ThreadLocations().execute();

    }

    private String loadJSONAssets() {
        return ZipManager.loadJson(context);
    }

    private ArrayList<Locations> loadLocations(String json) {
        ArrayList<Locations> locationsArrayList = new ArrayList<Locations>();
        try {
            if (json != null) {
                JSONObject data = new JSONObject(json);
                JSONObject objectUbicaciones = data.optJSONArray("data").getJSONObject(0);

                JSONArray ubicacionesArray = objectUbicaciones.optJSONArray("UBICACIONES");

                for (int i = 0; i < ubicacionesArray.length(); i++) {
                    JSONObject uObj = ubicacionesArray.getJSONObject(i);
                    Locations locations = new Locations(
                            uObj.optInt("FIIDUBICACION"),
                            uObj.optInt("FIIDCODIGOPOSTAL"),
                            uObj.optString("FCNOMBRE"),
                            uObj.optString("FCCLAVEUBICACION"),
                            uObj.optString("FCDIRECCION"),
                            uObj.optDouble("FNLATITUD"),
                            uObj.optDouble("FNLONGITUD"),
                            uObj.optInt("FIGEOCERCA"),
                            uObj.optInt("FIIDESTATUS"),
                            uObj.optInt("FIIDPOLIGONO"));
                    locationsArrayList.add(locations);
                }
                Log.d(TAG, "Tamaño: " + locationsArrayList.size());
                return locationsArrayList;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error: " + e.getMessage());
            dataViewInterface.showError("Ubicaciones no disponibles");
            return null;
        }
    }

    private String messageError(Throwable e) {
        String message = "";
        try {
            if (e instanceof IOException) {
                message = "Sin conexión a internet.";
            } else if (e instanceof HttpException) {
                HttpException error = (HttpException) e;
                String errorBody = error.response().errorBody().string();
                JSONObject jObj = new JSONObject(errorBody);

                message = jObj.getString("error");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        if (TextUtils.isEmpty(message)) {
            message = "Error desconocido";
        }
        return message;
    }

    private String unzipFile(String path) {
        File sd = Environment.getExternalStorageDirectory();
        if (sd.canWrite()) {
            return unzip(path, context.getFilesDir().getAbsolutePath().concat("/unzips"));
        }
        return "-1";
    }

    private class SaveFileZip extends AsyncTask<ResponseBody, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(ResponseBody... responseBodies) {
            String path = context.getFilesDir().getAbsolutePath().concat("/zips");
            return saveToDisk(responseBodies[0], path);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean)
                unzipF();
            else
                dataViewInterface.showError("Ubicaciones no disponibles");
        }
    }

    private class UnzipFileDownload extends AsyncTask<String, Integer, String>{
        @Override
        protected String doInBackground(String... strings) {
            String pathFile = unzipFile(strings[0]);

            return pathFile;
        }

        @Override
        protected void onPostExecute(String pathFile) {
            if (pathFile != null && !pathFile.equals("-1")) {
                String json = loadFile(pathFile);
                if (json != null)
                    dataViewInterface.showData(loadLocations(json));
                else
                    dataViewInterface.showError("Ubicaciones no disponibles");
            }
        }
    }

    private class ThreadLocations extends AsyncTask<Void, Integer, String>{
        @Override
        protected String doInBackground(Void... voids) {
            String json = loadJSONAssets();
            return json;
        }

        @Override
        protected void onPostExecute(String json) {
            if (json != null) {
                dataViewInterface.showLocationsFromAssets(loadLocations(json));
            } else {
                dataViewInterface.showError("Ubicaciones no disponibles");
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            //threadViewInterface.setProgressThread(values[0]);
        }
    }

    private void showStatus(Activity parent, final String message) {
        parent.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dataViewInterface.showStatus(message, -1);
            }
        });
    }


}
