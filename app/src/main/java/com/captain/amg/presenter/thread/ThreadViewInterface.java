package com.captain.amg.presenter.thread;

public interface ThreadViewInterface {

    void showResponse(String response);
    void setProgressThread(int progress);
}
