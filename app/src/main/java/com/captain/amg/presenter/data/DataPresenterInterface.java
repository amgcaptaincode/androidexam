package com.captain.amg.presenter.data;

import android.app.Activity;

import com.captain.amg.model.RequestData;

public interface DataPresenterInterface {

    void getData(Activity parent, RequestData requestData);

    void downloadFileZip(String urlFile);

    void getLocationsFromAssets(Activity parent);

}
