package com.captain.amg.presenter.data;

import com.captain.amg.model.Locations;

import java.util.ArrayList;

public interface DataViewInterface {

    void showData(ArrayList<Locations> locations);
    void showStatus(String status, int progress);
    void showError(String message);
    void showLocationsFromAssets(ArrayList<Locations> locations);
}
