package com.captain.amg.network;

import com.captain.amg.model.RequestData;
import com.captain.amg.model.ResponseData;

import io.reactivex.Single;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface NetworkInterface {

    @POST("getData")
    Single<ResponseData> getData(@Body RequestData requestData);

    @Streaming
    @GET
    Single<ResponseBody> downloadZipFile(@Url String urlZip);
}
