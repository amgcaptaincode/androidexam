package com.captain.amg.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.captain.amg.R;
import com.captain.amg.persistence.roomdatabase.entity.PersonalData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.captain.amg.manager.Toolbox.dateToString;

public class PersonalDataListAdapter extends RecyclerView.Adapter<PersonalDataListAdapter.DataViewHolder> {

    private final LayoutInflater mInflater;
    private List<PersonalData> mPersonalData; // Cached copy of words

    public PersonalDataListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = mInflater.inflate(R.layout.personal_data_item, parent, false);
        return new DataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder dataViewHolder, int position) {
        if (mPersonalData != null) {
            PersonalData current = mPersonalData.get(position);
            dataViewHolder.tvName.setText(current.getName());
            dataViewHolder.tvPost.setText(current.getPost());
            dataViewHolder.tvBirthdate.setText(current.getBirthdate() != null ? dateToString(current.getBirthdate()) : "");
        } else {
            // Covers the case of data not being ready yet
            dataViewHolder.tvName.setText("Sin datos");
        }
    }

    @Override
    public int getItemCount() {
        return mPersonalData != null ? mPersonalData.size() : 0;
    }

    public void setPersonalData(List<PersonalData> personalData) {
        mPersonalData = personalData;
        notifyDataSetChanged();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;

        @BindView(R.id.tvPost)
        TextView tvPost;

        @BindView(R.id.tvBirthdate)
        TextView tvBirthdate;

        public DataViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
