package com.captain.amg.view.activities;

import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.captain.amg.R;
import com.captain.amg.manager.ZipManager;
import com.captain.amg.model.Locations;
import com.captain.amg.presenter.data.DataPresenter;
import com.captain.amg.presenter.data.DataViewInterface;
import com.captain.amg.view.fragments.LocationsFragment;
import com.captain.amg.view.fragments.MapFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceActivity extends AppCompatActivity {

    private static final String TAG = ServiceActivity.class.getSimpleName();


    Fragment currentFragment;

    AlertDialog alertDialog = null;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private ArrayList<Locations> locationsArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupFragments(savedInstanceState);
    }
    private void setupFragments(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            currentFragment = new LocationsFragment();
            changeFragment(currentFragment);
        }
    }

    private void changeFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container_locations, fragment).commit();
    }

}
