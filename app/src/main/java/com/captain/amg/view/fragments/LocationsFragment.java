package com.captain.amg.view.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.captain.amg.R;
import com.captain.amg.model.Locations;
import com.captain.amg.model.RequestData;
import com.captain.amg.presenter.data.DataPresenter;
import com.captain.amg.presenter.data.DataViewInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LocationsFragment extends Fragment implements OnMapReadyCallback, DataViewInterface {

    private static final String TAG = LocationsFragment.class.getSimpleName();

    private View rootView;
    private GoogleMap gMap;
    private Marker marker;
    private CameraPosition camera;

    private DataPresenter mDataPresenter;

    @BindView(R.id.mapUbicaciones)
    MapView mapUbicaciones;
    @BindView(R.id.coordinatorLayoutLocations)
    CoordinatorLayout coordinatorLayout;

    AlertDialog alertDialog = null;

    public LocationsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_ubicaciones, container, false);
        ButterKnife.bind(this, rootView);
        setupPresenter();
        getDataWS();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapUbicaciones = (MapView) rootView.findViewById(R.id.mapUbicaciones);
        if (mapUbicaciones != null) {
            mapUbicaciones.onCreate(null);
            mapUbicaciones.onResume();
            mapUbicaciones.getMapAsync(this);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        cameraPosition();
    }

    private void setupPresenter() {
        mDataPresenter = new DataPresenter(this, getActivity());
    }

    private void getDataWS() {
        //mDataPresenter.getLocationsFromAssets();

        mDataPresenter.getData(getActivity(), new RequestData("55", "12984"));
    }

    @Override
    public void showData(ArrayList<Locations> locations) {
        createMarkers(locations);
    }

    @Override
    public void showStatus(String status, int progress) {
        showDialogStatus(status, progress);
    }

    @Override
    public void showError(String message) {
        alertDialog.dismiss();
        showSnackBar(message);
        switch (message) {
            case "Sin conexión a internet.":
            case "Error desconocido":
                mDataPresenter.getLocationsFromAssets(getActivity());
                break;
        }
    }

    @Override
    public void showLocationsFromAssets(ArrayList<Locations> locations) {
        createMarkers(locations);
    }

    private void createMarkers(ArrayList<Locations> locations) {
        for(int i = 0; i < locations.size(); i++) {
            Locations ubicacion = locations.get(i);
            LatLng latLng = new LatLng(ubicacion.getFnLatitud(), ubicacion.getFnLongitud());
            gMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .draggable(false)
                    .title(String.format("%s", ubicacion.getFcNombre()))
                    .snippet(String.format("%s", ubicacion.getFcDireccion() )));
        }
        alertDialog.dismiss();
    }

    private void showDialogStatus(final String status, final int progress) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.status_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(view);

        TextView tvStatus = view.findViewById(R.id.tvStatus);
        ProgressBar progressBar = view.findViewById(R.id.progressBar);

        tvStatus.setText(status);

        alertDialogBuilder
                .setCancelable(false);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void cameraPosition() {
        camera = new CameraPosition.Builder()
                .target(new LatLng(23, -102))
                .zoom(5)           // limit -> 21
                .bearing(0)         // 0 - 365º
                .tilt(30)           // limit -> 90
                .build();
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));

    }

    private void showSnackBar(String message) {
        Log.d(TAG, message);
        //Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

}
