package com.captain.amg.view.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.captain.amg.R;
import com.captain.amg.presenter.thread.ThreadPresenter;
import com.captain.amg.presenter.thread.ThreadViewInterface;

public class ThreadActivity extends AppCompatActivity implements View.OnClickListener, ThreadViewInterface {

    private LinearLayout myLInearLayout;
    private Button btnThread;
    private TextView tvText;

    private ThreadPresenter mThreadPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createLayout();

        //set Content View
        setContentView(myLInearLayout);

        createViews();

        setupPresenter();

        customViews();

        addViewToLayout();

        initListeners();

    }

    private void createLayout() {
        //create LInearLayout
        myLInearLayout = new LinearLayout(getApplicationContext());

        //add LayoutParams
        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);

        myLInearLayout.setLayoutParams(params);

        myLInearLayout.setOrientation(LinearLayout.VERTICAL);
    }

    private void setupPresenter() {
        mThreadPresenter = new ThreadPresenter(this);
    }

    private void createViews() {
        // add Button
        btnThread = new Button(this);
        tvText = new TextView(this);
    }

    private void customViews() {
        btnThread.setLayoutParams(
                new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));

        btnThread.setBackgroundResource(R.drawable.btn_custom);
        btnThread.setPadding(10, 10, 10, 10);
        btnThread.setTextColor(getResources().getColor(android.R.color.white));
        btnThread.setTextSize(18);
        btnThread.setText(getString(R.string.txt_task_execute));
        btnThread.setId(5);

        tvText.setLayoutParams(
                new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
        tvText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tvText.setGravity(Gravity.CENTER);
        tvText.setTextSize(30);
        tvText.setId(5);

    }

    private void addViewToLayout() {
        //add the textView and the Button to LinearLayout
        myLInearLayout.addView(btnThread);
        myLInearLayout.addView(tvText);
    }

    private void initListeners() {
        btnThread.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case 5:
                mThreadPresenter.executeThread(10);
                btnThread.setEnabled(false);
                break;
        }
    }

    @Override
    public void showResponse(String response) {
        tvText.setText("");
        openDialog(response);
        btnThread.setEnabled(true);
    }

    @Override
    public void setProgressThread(int progress) {
        tvText.setText(String.format(getString(R.string.text_progress), progress));
    }

    public void openDialog(String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        TextView tvTitle = new TextView(this);
        tvTitle.setText(message);
        tvTitle.setPadding(10, 20, 10, 20);
        tvTitle.setGravity(Gravity.CENTER);
        tvTitle.setTextColor(Color.BLACK);
        tvTitle.setTextSize(20);
        alertDialog.setCustomTitle(tvTitle);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,getString(R.string.text_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });

        new Dialog(getApplicationContext());
        alertDialog.show();
    }
}
