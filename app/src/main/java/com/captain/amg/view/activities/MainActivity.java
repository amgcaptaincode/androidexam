package com.captain.amg.view.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.captain.amg.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.captain.amg.manager.Constants.DELAY;
import static com.captain.amg.manager.Constants.REQUEST_ID_MULTIPLE_PERMISSIONS;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.btnActivityOne)      Button btnActivityOne;
    @BindView(R.id.btnActivityTwo)      Button btnActivityTwo;
    @BindView(R.id.btnActivityThree)    Button btnActivityThree;
    @BindView(R.id.btnActivityFour)     Button btnActivityFour;


    private String[] permissions = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        handlerPermission();
    }

    @OnClick({R.id.btnActivityOne, R.id.btnActivityTwo, R.id.btnActivityThree, R.id.btnActivityFour})
    public void OnClickButtons(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.btnActivityOne:
                intent = new Intent(MainActivity.this, MapsActivity.class);
                break;
            case R.id.btnActivityTwo:
                intent = new Intent(MainActivity.this, ServiceActivity.class);
                break;
            case R.id.btnActivityThree:
                intent = new Intent(MainActivity.this, ThreadActivity.class);
                break;
            case R.id.btnActivityFour:
                intent = new Intent(MainActivity.this, PersistenceActivity.class);
                break;
        }
        startActivity(intent);
    }

    private void handlerPermission() {
        Handler handler = new Handler();
        handler.postDelayed(initMain(), DELAY);
    }

    private Runnable initMain() {
        Runnable res = new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermissions()) {
                        Toast.makeText(MainActivity.this, "Permisos conceditos", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        return res;
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Handler handler = new Handler();
                    handler.postDelayed(initMain(), DELAY);
                } else {
                    finish();
                }
                break;
        }
    }



}
