package com.captain.amg.view.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.captain.amg.R;
import com.captain.amg.persistence.roomdatabase.entity.PersonalData;
import com.captain.amg.view.adapters.PersonalDataListAdapter;
import com.captain.amg.viewmodel.PersonalDataViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.captain.amg.manager.Toolbox.getDate;

public class PersistenceActivity extends AppCompatActivity {

    private PersonalDataViewModel mPersonalDataViewModel;
    private List<PersonalData> mDataList;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persistence);
        ButterKnife.bind(this);

        final PersonalDataListAdapter adapter = new PersonalDataListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mPersonalDataViewModel = ViewModelProviders.of(this).get(PersonalDataViewModel.class);

        mPersonalDataViewModel.getAllData().observe(this, new Observer<List<PersonalData>>() {
            @Override
            public void onChanged(@Nullable List<PersonalData> personalData) {
                count = personalData != null ? personalData.size() : 0;
                adapter.setPersonalData(personalData);
            }
        });
        setupListData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.fab)
    public void OnClickFab(View view) {
        if (count <= 3) {
            if (count == 3)
                fab.setImageDrawable(ContextCompat
                        .getDrawable(getApplicationContext(), R.drawable.ic_delete_sweep));
            mPersonalDataViewModel.insert(mDataList.get(count));
            count++;
        } else {
            count = 0;
            mPersonalDataViewModel.deleteAll();
            fab.setImageDrawable(ContextCompat
                    .getDrawable(getApplicationContext(), R.drawable.ic_person_add));
        }


    }

    private void setupListData() {
        mDataList = new ArrayList<PersonalData>(){};
        mDataList.add(new PersonalData("Miguel Cervantes", getDate("08-Dic-1990"), "Desarrollador"));
        mDataList.add(new PersonalData("Juan Morales", getDate("03-Jul-1990"), "Desarrollador"));
        mDataList.add(new PersonalData("Roberto Méndez", getDate("14-Oct-1990"), "Desarrollador"));
        mDataList.add(new PersonalData("Miguel Cuevas", getDate("08-Dic-1990"), "Desarrollador"));

    }

}
