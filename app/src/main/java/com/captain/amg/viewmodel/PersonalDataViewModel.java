package com.captain.amg.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.captain.amg.persistence.repository.PersonalDataRepository;
import com.captain.amg.persistence.roomdatabase.entity.PersonalData;

import java.util.List;

public class PersonalDataViewModel extends AndroidViewModel {

    private PersonalDataRepository mRepository;
    private LiveData<List<PersonalData>> mAllData;

    public PersonalDataViewModel(@NonNull Application application) {
        super(application);
        mRepository = new PersonalDataRepository(application);
        mAllData = mRepository.getAllData();
    }

    public LiveData<List<PersonalData>> getAllData() {
        return mAllData;
    }

    public void insert(PersonalData personalData) {
        mRepository.insert(personalData);
    }

    public void deleteAll() {
        mRepository.deleteAll();
    }


}
